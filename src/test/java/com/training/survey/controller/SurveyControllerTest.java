package com.training.survey.controller;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.bind.annotation.RequestBody;

import com.training.survey.dto.ResponseDto;
import com.training.survey.dto.SurveyCustomerDto;
import com.training.survey.dto.SurveyDto;
import com.training.survey.dto.SurveyResponseDto;
import com.training.survey.entity.Survey;
import com.training.survey.service.SurveyService;

import jakarta.validation.Valid;
@ExtendWith(SpringExtension.class)
 class SurveyControllerTest {
	@Mock
	SurveyService surveyService;
	@InjectMocks
	SurveryController surveyController;
	

	
	@Test
	void testSucess() {
		SurveyDto surveyDto = new SurveyDto();
		
		SurveyCustomerDto customerDto1=new SurveyCustomerDto();
		customerDto1.setCustomerName("darshan");
		
		SurveyCustomerDto customerDto2=new SurveyCustomerDto();
		customerDto2.setCustomerName("darshan");
		List<SurveyCustomerDto> list=new ArrayList<>();
		list.add(customerDto1);
		list.add(customerDto2);
		
		SurveyResponseDto responseDto=new SurveyResponseDto();
		responseDto.setBranchId(1l);
		responseDto.setCustomerDtos(list);
	
		Mockito.when(surveyService.surveyDetails(any(SurveyDto.class))).thenReturn(responseDto);
		
		ResponseEntity<SurveyResponseDto> response = surveyController.customerSurvey(surveyDto);
		assertEquals(HttpStatusCode.valueOf(201), response.getStatusCode());
	}

}

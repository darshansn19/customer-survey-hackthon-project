package com.training.survey.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.training.survey.dto.CustomerDto;
import com.training.survey.dto.ResponseDto;
import com.training.survey.entity.Customer;
import com.training.survey.entity.Gender;
import com.training.survey.exception.CustomerAlreadyExists;
import com.training.survey.repository.CustomerRepository;

@ExtendWith(SpringExtension.class)
class CustomerServiceImplTest {

	@InjectMocks
	CustomerServiceImpl customerServiceImpl;

	@Mock
	CustomerRepository customerRepository;

	@Test
	void addCustomer_PositiveTest() {

		Customer customer = new Customer();
		customer.setCustomerId(1L);

		CustomerDto customerDto = new CustomerDto("chaitra", "shetty", "FEMALE", "Bengaluru");

		when(customerRepository.findByFirstName(customerDto.firstName())).thenReturn(Optional.empty());
		ResponseDto response = customerServiceImpl.addCustomer(customerDto);
		assertNotNull(response);
		assertEquals(200, response.httpStatusCode());
		assertEquals("Customer added successfully", response.message());

	}
	
	@Test
	void customerAlreadyExists_PositiveTest() {

		Customer customer = new Customer();
		customer.setCustomerId(1L);
		customer.setFirstName("chaitra");
		customer.setLastName("shetty");
		customer.setGender(Gender.FEMALE);
		customer.setAddress("FEMALE");

		CustomerDto customerDto = new CustomerDto("chaitra", "shetty", "FEMALE", "FEMALE");

		when(customerRepository.findByFirstName(customerDto.firstName())).thenReturn(Optional.of(customer));
		CustomerAlreadyExists exception = assertThrows(CustomerAlreadyExists.class,
				() -> customerServiceImpl.addCustomer(customerDto));
		assertEquals("Customer Already exists", exception.getMessage());
 

		
		

	}

}


	



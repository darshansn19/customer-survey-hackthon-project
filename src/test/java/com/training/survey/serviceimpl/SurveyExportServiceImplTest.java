package com.training.survey.serviceimpl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.training.survey.dto.SurveyExportDto;
import com.training.survey.entity.Branch;
import com.training.survey.entity.Customer;
import com.training.survey.entity.Gender;
import com.training.survey.entity.Survey;
import com.training.survey.repository.SurveyRepository;
import com.training.survey.service.impl.SurveyExportServiceImpl;

@ExtendWith(SpringExtension.class)
class SurveyExportServiceImplTest {

	@Mock
	private SurveyRepository surveyRepository;

	@InjectMocks
	private SurveyExportServiceImpl surveyExportServiceImpl;

	@Test
	void exportSurveyDetailssuccessTest() {

		Branch branch = new Branch();
		branch.setBranchId(1l);
		branch.setBranchAddress("bng");

		Customer customer1 = new Customer();
		customer1.setCustomerId(1l);
		customer1.setFirstName("darshan");
		customer1.setGender(Gender.MALE);
		customer1.setAddress("adds");

		Customer customer2 = new Customer();
		customer2.setCustomerId(2l);
		customer2.setFirstName("vinay");
		customer2.setGender(Gender.MALE);
		customer2.setAddress("sds");

		List<Survey> list = new ArrayList<>();

		Survey survey1 = new Survey();
		survey1.setSurveyId(1l);
		survey1.setBranch(branch);
		survey1.setCustomer(customer1);
		survey1.setAddress("adds");

		Survey survey2 = new Survey();
		survey2.setSurveyId(2l);
		survey2.setBranch(branch);
		survey2.setCustomer(customer2);
		survey2.setAddress("sds");

		list.add(survey1);
		list.add(survey2);

		Mockito.when(surveyRepository.findAll()).thenReturn(list);
		List<SurveyExportDto> dtos = surveyExportServiceImpl.exportSurveyDetails();

		assertNotNull(dtos);
		assertEquals(list.size(), dtos.size());
	}
	
	@Test
	void exportSurveyDetailsForNull()
	{
		Mockito.when(surveyRepository.findAll()).thenReturn(null);
		assertThrows(NullPointerException.class, () -> {
			surveyExportServiceImpl.exportSurveyDetails();
		});
	}
}

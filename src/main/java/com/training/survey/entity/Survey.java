package com.training.survey.entity;

import org.hibernate.annotations.ManyToAny;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Survey {

	@Id
	@GeneratedValue(strategy =  GenerationType.IDENTITY)
	private Long surveyId;
	
	@ManyToOne
	@JoinColumn(name = "branchId")
	private Branch branch;
	

	@ManyToOne
	@JoinColumn(name = "customerId")
	private Customer customer;

	private String address;
	private Double distance;
}

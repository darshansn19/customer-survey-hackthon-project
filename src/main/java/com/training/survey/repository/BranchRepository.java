package com.training.survey.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.training.survey.entity.Branch;

import jakarta.validation.constraints.NotNull;

@Repository
public interface BranchRepository extends JpaRepository<Branch, Long>{

	Branch findByBranchId(@NotNull(message = "branchID should not be null") Long branchId);

}

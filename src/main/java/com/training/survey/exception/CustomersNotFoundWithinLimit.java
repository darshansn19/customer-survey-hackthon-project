package com.training.survey.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomersNotFoundWithinLimit extends RuntimeException{


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final String message;
	public CustomersNotFoundWithinLimit(String message) {
		super();
		this.message = message;
	}
	
}

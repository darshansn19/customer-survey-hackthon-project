package com.training.survey.dto;

import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class SurveyDto{
	@NotNull(message = "branchID should not be null")
	Long branchId;
	@NotNull(message = "distance should not be null")
	Double distance;

}

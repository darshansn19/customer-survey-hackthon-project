package com.training.survey.dto;

import lombok.Data;

@Data
public class SurveyExportDto {

	
	private Long BranchId;
	private String customerName;
	private String Address;
	private Double distance;
}

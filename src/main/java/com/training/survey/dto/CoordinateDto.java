package com.training.survey.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CoordinateDto {

    private double latitude;
    private double longitude;
}

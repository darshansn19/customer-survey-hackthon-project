package com.training.survey.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.training.survey.dto.CustomerDto;
import com.training.survey.dto.ResponseDto;
import com.training.survey.service.CustomerService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class CustomerController {
	@Autowired
	private CustomerService customerService;
	@PostMapping("/customers")
	public ResponseEntity<ResponseDto> addCustomer(@RequestBody CustomerDto customerDto)
	{
		return new ResponseEntity<ResponseDto>(customerService.addCustomer(customerDto), HttpStatus.CREATED);
	}

}



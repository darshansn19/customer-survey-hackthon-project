package com.training.survey.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.training.survey.dto.SurveyDto;
import com.training.survey.dto.SurveyResponseDto;
import com.training.survey.service.SurveyService;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class SurveryController {
	private final SurveyService surveyService;

	@PostMapping("/survey")
	public ResponseEntity<SurveyResponseDto> customerSurvey(@Valid @RequestBody SurveyDto surveyDto) {
		return new ResponseEntity<>(surveyService.surveyDetails(surveyDto), HttpStatus.CREATED);
	}
}

package com.training.survey.controller;

import java.io.IOException;

import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.opencsv.ICSVWriter;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import com.training.survey.dto.SurveyExportDto;
import com.training.survey.service.SurveyExportService;

import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class SurveyExportController {

	private final SurveyExportService surveyExportService;

	@GetMapping("/survey")
	public void exportCustomerDetails(HttpServletResponse response)
			throws IOException, CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {

		String fileName = "customerDetails.csv";

		response.setContentType("text/csv");
		response.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fileName + "\"");

		StatefulBeanToCsv<SurveyExportDto> writer = new StatefulBeanToCsvBuilder<SurveyExportDto>(response.getWriter())
				.withQuotechar(ICSVWriter.NO_QUOTE_CHARACTER).withSeparator(ICSVWriter.DEFAULT_SEPARATOR)
				.withOrderedResults(true).build();

		writer.write(surveyExportService.exportSurveyDetails());

	}
	
}

package com.training.survey;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class CustomerSurveyApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomerSurveyApplication.class, args);
	}

}

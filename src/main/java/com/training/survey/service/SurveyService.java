package com.training.survey.service;

import com.training.survey.dto.SurveyDto;
import com.training.survey.dto.SurveyResponseDto;

public interface SurveyService {

	SurveyResponseDto surveyDetails(SurveyDto surveyDto);
}

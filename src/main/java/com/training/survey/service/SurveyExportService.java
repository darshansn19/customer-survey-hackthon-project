package com.training.survey.service;

import java.util.List;

import com.training.survey.dto.SurveyExportDto;

public interface SurveyExportService {

	
	List<SurveyExportDto> exportSurveyDetails();

	


}

package com.training.survey.service.impl;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.training.survey.dto.CoordinateDto;



@FeignClient(url = "http://localhost:8080/hackathon/coordinate-service/get-by-address", value = "coordinate-service")
public interface CoordinateService {

	@GetMapping
	CoordinateDto getCoordinate(@RequestParam String address);
}


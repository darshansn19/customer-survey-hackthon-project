package com.training.survey.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.springframework.stereotype.Service;

import com.training.survey.dto.CoordinateDto;
import com.training.survey.dto.SurveyCustomerDto;
import com.training.survey.dto.SurveyDto;
import com.training.survey.dto.SurveyResponseDto;
import com.training.survey.entity.Branch;
import com.training.survey.entity.Customer;
import com.training.survey.entity.Survey;
import com.training.survey.exception.BranchNotFoundException;
import com.training.survey.exception.CustomersNotFoundWithinLimit;
import com.training.survey.repository.BranchRepository;
import com.training.survey.repository.CustomerRepository;
import com.training.survey.repository.SurveyRepository;
import com.training.survey.service.SurveyService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@RequiredArgsConstructor
public class SurveyServiceImpl implements SurveyService {

	private final CoordinateService coordinateService;
	private final BranchRepository branchRepository;
	private final CustomerRepository customerRepository;
	private final SurveyRepository surveyRepository;

	@Override
	public SurveyResponseDto surveyDetails(SurveyDto surveyDto) {

		Branch branch = branchRepository.findByBranchId(surveyDto.getBranchId());

		if (Objects.isNull(branch)) {
			log.warn("Branch Not Found");
			throw new BranchNotFoundException("Branch Not Found");
		}

		CoordinateDto branchCoordinates = coordinateService.getCoordinate(branch.getBranchAddress());

		CoordinateDto customerCoordinates;
		Survey survey = null;
		SurveyCustomerDto surveyCustomerDto;

		SurveyResponseDto reponseDto = new SurveyResponseDto();
		reponseDto.setBranchId(branch.getBranchId());
		List<SurveyCustomerDto> surveyCustomerList = new ArrayList<>();

		List<Survey> surveyList = new ArrayList<>();
		List<Customer> customers = customerRepository.findAll();

		for (Customer customer : customers) {

			customerCoordinates = coordinateService.getCoordinate(customer.getAddress());
			Double distance = distance(branchCoordinates.getLatitude(), branchCoordinates.getLongitude(),
					customerCoordinates.getLatitude(), customerCoordinates.getLongitude(), "K");
			if (distance > surveyDto.getDistance())
				break;

			survey = new Survey();
			survey.setDistance(distance);
			survey.setAddress(customer.getAddress());
			survey.setBranch(branch);
			survey.setCustomer(customer);
			surveyList.add(survey);

			surveyCustomerDto = new SurveyCustomerDto();
			surveyCustomerDto.setCustomerAddress(customer.getAddress());
			surveyCustomerDto.setCustomerName(customer.getFirstName());
			surveyCustomerDto.setDistance(distance);
			surveyCustomerList.add(surveyCustomerDto);

		}
		if (Objects.equals(surveyCustomerList.size(), 0)) {
			log.warn("Customer not found within the given distance limit: " + surveyDto.getDistance());
			throw new CustomersNotFoundWithinLimit("Customer not found within the given distance limit");
		}
		surveyRepository.saveAll(surveyList);
		reponseDto.setCustomerDtos(surveyCustomerList);
		return reponseDto;

	}

	public static double distance(double lat1, double lon1, double lat2, double lon2, String unit) {
		if ((lat1 == lat2) && (lon1 == lon2)) {
			return 0;
		} else {
			double theta = lon1 - lon2;
			double dist = Math.sin(Math.toRadians(lat1)) * Math.sin(Math.toRadians(lat2))
					+ Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) * Math.cos(Math.toRadians(theta));

			// formula to calculate the distance between two
			// acos(sin(lat1)*sin(lat2)+cos(lat1)*cos(lat2)*cos(lon2-lon1))*6371
			dist = Math.acos(dist);
			dist = Math.toDegrees(dist);

			// earth radius
			dist = dist * 60 * 1.1515;
			if (unit.equals("K")) {
				dist = dist * 1.609344;
			} else if (unit.equals("N")) {
				dist = dist * 0.8684;
			}
			return (dist);
		}
	}

}

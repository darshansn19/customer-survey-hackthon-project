package com.training.survey.service.impl;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.training.survey.dto.CustomerDto;
import com.training.survey.dto.ResponseDto;
import com.training.survey.entity.Customer;
import com.training.survey.entity.Gender;
import com.training.survey.exception.CustomerAlreadyExists;
import com.training.survey.repository.CustomerRepository;
import com.training.survey.service.CustomerService;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
@Slf4j
@Service
@RequiredArgsConstructor
public class CustomerServiceImpl implements CustomerService{
	private final CustomerRepository customerRepository;
	@Override
	public ResponseDto addCustomer(@Valid CustomerDto customerDto) {
		{
			Optional<Customer> customer = customerRepository.findByFirstName(customerDto.firstName());
			if(customer.isPresent())
			{
				log.error("Customer Already exists");
				throw new CustomerAlreadyExists("Customer Already exists");
			}
			Customer customers = new Customer();
			BeanUtils.copyProperties(customerDto, customers);
			customers.setGender(Gender.valueOf(customerDto.gender()));
		     customerRepository.save(customers);
			log.info("Customer added sucessfully");
			return new ResponseDto("Customer added successfully", 200);
		}
		

	}
	

	
	

}